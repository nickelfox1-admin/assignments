#Ticket Counter
def payment(a, b):
    if a > 10:
        return a*20
    else:
        return (a*20)+(b*10)  # Rs.20 is the Ticket cost for Adults and Rs. 10 for children


while True:
    try:
        adults = int(input('enter the number of adults '))
    except ValueError:
        print("please enter a valid value")
        continue
    else:
        if adults < 0:
            print("cannot be negative")
            continue
    break

while True:
    try:
        children = int(input('enter the number of children '))
    except ValueError:
        print("please enter a valid value")
        continue
    else:
        if children < 0:
            print("cannot be negative")
            continue
    break


children_small = input('are there any children below 5 years of age, type "y" for yes or any other key for no ')
if children_small == "y":
    while True:
        try:
            child_b5 = int(input("enter the number of children below five "))
        except ValueError:
            print("please enter a valid value")
            continue
        else:
            if child_b5 < 0:
                print("cannot be negative")
                continue
        break

else:
    child_b5 = 0

eff_children = children - child_b5
effective = adults + eff_children
ticket_cost = payment(adults, eff_children)
print('the total amount to be paid is:', ticket_cost)
