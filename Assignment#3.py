#Vaccination Center Appointment
import json

with open('capacity.json') as f:
    data = json.load(f)

# to calculate total combined capacity of given centers on all available days
sum = 0
for entry in data:
    for center in entry['centers']:
        sum = sum + entry['centers'][center]

while True:
    num = int(input("enter the number of Patients "))
    if num <= sum:
        break
    else:
        print("cannot be greater than total combined capacity of all centers")

# To Allocate Vaccination centers to patients
list1 = []
for entry in data:
    mydict = {}
    tempdict = {}
    for center in entry['centers']:
        temp = entry['centers'][center]
        dateToday = entry['date']
        mydict['date'] = dateToday
        if num > temp:
            num = num-temp
            print(f'{temp} number of patients have been scheduled in center {center} on {dateToday}')
            tempdict[center] = temp
        else:
            print(f'{num} number of patients have been scheduled in center {center} on {dateToday}')
            tempdict[center] = num
            num = 0
            break
    mydict['centers'] = tempdict
    list1.append(mydict)
    if num == 0:
        break

with open('new capacity.json', 'w') as f:
    json.dump(list1, f, indent=2)
