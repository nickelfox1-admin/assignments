#Pizza Outlet
class Pizza:
    def __init__(self, x):
        if x > 10:
            return
        else:
            self.x = x

    def price_calculator(self):
        mapper = {
            1:{'pizza1': 30},
            2:{'pizza2': 40},
            3:{'pizza3': 50},
            4:{'pizza4': 60},
            5:{'pizza5': 70},
            6:{'pizza6': 80},
            7:{'pizza7': 90},
            8:{'pizza8': 100},
            9:{'pizza9': 110},
            10:{'pizza10': 120},
        }

        z = mapper.get(self.x, 0)
        for x in z:
            return z[x]


class Toppings:
    def __init__(self, x):
        self.x = x

    def price_calculator(self):
        mapper = {
            1:{'Pepperoni': 10},
            2:{'Onions': 20},
            3:{'Extra cheese': 30},
            4:{'Black olives': 40},
            5:{'Sausage': 50},
        }

        z = mapper.get(self.x, 0)
        for x in z:
            return z[x]


class Coupon:
    def __init__(self, x):
        self.x = x

    def flat10_off(self):
        if self.x == 'y':
            return -10
        else:
            return 0


while True:
    try:
        p = int(input('enter the pizza type you wish to order: '
              '1 for pizza1, 2 for pizza2, 3 for pizza3 and So on upto 10 '))

    except ValueError:
        print("please enter a valid value")
        continue
    else:
        if p > 10 or p <= 0:
            print('enter only from 1 to 10')
            continue
    pizza = Pizza(p)
    break

while True:
    try:
        t = int(input('enter the topping type you wish to include: 1 for Pepperoni, 2 for Onions, 3 for Extra cheese, '
              '4 for Black Olives and 5 for Sausage '))
    except ValueError:
        print("please enter a valid value")
        continue
    else:
        if t > 5 or t <= 0:
            print('enter only from 1 to 5')
            continue
    topping = Toppings(t)
    break


c = input('do you want the coupon to be applied "y" for yes and "n" for NO ')
coupon = Coupon(c)

total_price = pizza.price_calculator() + topping.price_calculator() + coupon.flat10_off()
print('the total amount you have to pay is:', total_price)